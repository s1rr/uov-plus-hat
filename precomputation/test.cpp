#include <stdio.h>
#include <gmp.h>
#include <NTL/ZZ.h>
#include <NTL/ZZ_pXFactoring.h>
#include <NTL/vector.h>
#include <NTL/matrix.h>


// g++ -g -O2 -std=c++11 -pthread -march=native main.cpp -o main -lntl -lgmp -lm

using namespace std;
using namespace NTL;


/*
  structs: ZZX (pols), Mat<T> (Vec<Vec<T>>),
 */

// int PoltoMatrix(pol p, );



int main (int argc, char** argv) {
    ZZ p;
    // cin >> p; // wait for input (ridiculous)
    p = 13;
    ZZ_p::init(p);

    for (int i = 1; i<=8; i++) {
	MQ_system[i].SetLength(8);
    }
    
    ZZ_pX f; // polynome f mod p
    int f_deg = 4;
    SetCoeff(f, 0, 1);
    f.SetLength(f_deg);
    f[1] = 3;
    cout << f;
    
    Mat<ZZ> X;

    // --- T --- //
    long D = 5;
    Vec<Mat<ZZ_p>> T_supp(INIT_SIZE, D); // T_supp[D][mons, pols]
    for (int i = 0; i < D; i++) {
	T_supp[i].SetDims(nbmons_MQsystem, nbpols_MQsystem); // dim'll depend on our choice
	for (int j = 0; j < nbmons_MQsystem; j++) {
	    for (int k = 0; k < nbpols_MQsystem; k++) {
		coef = randombytes_uniform(2);
		T_supp[i][j][k] = coef;
	    }
	}
    }
    cout << T_supp << "\n\n"; // DEBUG

    Mat<ZZ_p> T; // T[D, nbmons]
    T.SetDims(D, nbmons_MQsystem); // nbmons here will depend on our choice
    for (int i = 0; i < D; i++) {
	for (int j = 0; j < nbmons_MQsystem; j++) {
	    T[i][j] = randombytes_uniform(2);
	}
    }
    cout << T << "\n\n"; // DEBUG


    return 0;
}



    // for (int i = 0; i < 1; i++) { // une iteration pour le moment
    // 	M.SetDims(8, 8); // I need to change the dimension every time
    // 	for (int j = 0; j < len(T); j++) {
    // 	}
    // }
