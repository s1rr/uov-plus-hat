


def matrix_inv_weird(A):
    lencol = A.size();
    B = A[..lencol]
    reduce(B)
    mult(B^{-1}, A[lencol..])
    return A


def matrix_inv(A):
    return reduce(A)

