#include <stdio.h>
#include <gmp.h>
#include <gf2x.h>
#include <NTL/ZZ.h>
#include <NTL/ZZ_pXFactoring.h>
#include <NTL/vector.h>
#include <NTL/matrix.h>
// g++ -g -O2 -std=c++11 -pthread -march=native main.cpp -o main -lntl -lgmp -lm -lgf2x

using namespace std;
using namespace NTL;

int PivotsReduction(Mat<ZZ_p>& M, int GRows, int GColums);


int main (int argc, char** argv) {
    int p = 251;
    ZZ modulus = ZZ(p); // define Z/pZ
    ZZ_p::init(modulus);

    // Definition du MQ-system
    Vec<ZZ_pX> MQ_system(INIT_SIZE, 8);
    ZZ_p coef;
    coef = 0;
    long nbpols_MQsystem = 8;
    long nbmons_MQsystem = 8;
    long nbvars_MQsystem = nbpols_MQsystem; // systeme considere en vars huile
    for (int i = 0; i < nbpols_MQsystem; i++) {
	for (int j = 0; j < nbmons_MQsystem; j++) {
	    random(coef);
	    SetCoeff(MQ_system[i], j, coef);
	}
    }

    // cout << MQ_system << "\n\n"; // DEBUG

    // ----- PRE COMPUTED VALUES ---- //
    // Structure venant de Magma
    int D = 2;

    Vec<int> T_nbcoeffs(INIT_SIZE, D);
    Vec<Vec<Mat<int>>> T; // [D][nb_coeffs][2][2]
    for (int j; j < D; j++) {
	T[j].SetLength(T_nbcoeffs[j]);
	for (int k; j < T_nbcoeffs[j]; k++) {
	    T[j][k].SetDims(2, 2);
	}
    }
    Vec<int> T_pols(INIT_SIZE, D);

    Vec<int> PivotsPhase1_nbpivots(INIT_SIZE, D);
    Vec<Mat<int>> PivotsPhase1(INIT_SIZE, D);
    for (int j = 0; j < D; j++) {
	PivotsPhase1[j].SetDims(PivotsPhase1_nbpivots[j], 2);
    }

    Vec<int> PivotsPhase2_nbpivots(INIT_SIZE, D);
    Vec<Mat<int>> PivotsPhase2(INIT_SIZE, D);
    for (int j; j < D; j++) {
	PivotsPhase2[j].SetDims(PivotsPhase2_nbpivots[j], 2);
    }

    Vec<int> GRows(INIT_SIZE, D); // pour ajouter les \muG
    Vec<int> GColumns(INIT_SIZE, D);
    Vec<int> GRowsFinal(INIT_SIZE, D); // assemblage final de G (qui sera Gtemp) et G
    Vec<int> GColumnsFinal(INIT_SIZE, D);

    Vec<int> TFinal_nbcoeffs(INIT_SIZE, D);
    Vec<Vec<Mat<int>>> TFinal; // [D][nb_coeffs][2][2]
    for (int j; j < D; j++) {
	TFinal[j].SetLength(TFinal_nbcoeffs[j]);
	for (int k; j < TFinal_nbcoeffs[j]; k++) {
	    TFinal[j][k].SetDims(2, 2);
	}
    }
    Vec<int> TFinal_pols(INIT_SIZE, D);

    // ----- COMPUTATION ----- //

    Mat<ZZ_p> G; // Matrix(MQ_system), generation du systeme
    GRows[0] = 8;
    GColumns[0] = 8;
    G.SetDims(GRows[0], GColumns[0]);
    for (int i = 0; i < GRows[i]; i++) {
	for (int j = 0; j < GColumns[i]; j++) {
	    G[i][j] = coeff(MQ_system[i], j);
	}
    }

    cout << G << "\n"; // DEBUG

    long rank;
    rank = gauss(G);
    cout << "G: \n" << G << "\n" << "G rank: " << rank << "\n";; // DEBUG

    // Vars utilise dans la boucle
    Mat<ZZ_p> Gtemp; // Gtemporary matrix for G copy
    int indice0; // T
    int indice1;
    int indice2;
    int indice3;
    int nb_NonPivots; // Subsystem
    int Row_NoPivot;

    for (int i = 1; i < D; i++) { // i = 0 est la matrice de depart

	// Processus d'agrandissement de la matrice
	Gtemp.SetDims(GRows[i - 1], GColumns[i - 1]);
	for (int k = 0; k < GRows[i]; k++) { // copie G -> Gtemp
	    for (int l = 0; l < GColumns[i]; l++) {
		Gtemp[k][l] = G[k][l]; // on parcourt tout, donc pas de pb d'ancien coeff de Gtemp a i - 1
	    }
	}

	G.SetDims(GRows[i], GColumns[i]); // detruit G car G_{i}Rows > G_{i-1}Rows
	for (int k = 0; k < Gtemp.NumRows(); k++) { // copie Gtemp -> G
	    for (int l = 0; l < Gtemp.NumCols(); l++) {
		G[k][l] = Gtemp[k][l];
	    }
	}
	cout << "\n" << G << "\n"; // DEBUG


	// G \mapsto \mu G
	// T = [[[[i_1, j_1], [i_2, j_2]], ... ]]
	for (int j = 0; j < T[i].length(); j++) {
	    indice0 = T[i][j][0][0];
	    indice1 = T[i][j][0][1];
	    indice2 = T[i][j][1][0];
	    indice3 = T[i][j][1][1];
	    G[indice2][indice3] = G[indice0][indice1];
	}

	// Pivots reduction phase 1
	PivotsReduction(G, GRows[i], GColumns[i]);
	cout << "After pivot reduction\n" << G << "\n";

	// Select sub system
	Mat<ZZ_p> GRed;
	nb_NonPivots = GRows[i] - PivotsPhase1_nbpivots[i];
	Vec<int> GRedRowsNoPivot(INIT_SIZE, nb_NonPivots); // les lignes a selectionner // A DEFINIR
	GRed.SetDims(nb_NonPivots, GColumns[i]);
	for (int k = 0; k < nb_NonPivots; k++) {
	    for (int l = 0; l < GColumns[i]; l++) {
		Row_NoPivot = GRedRowsNoPivot[k];
		GRed[k][l] = G[Row_NoPivot][l];
	    }
	}

	rank = gauss(GRed);
	cout << "GRed: \n" << G << "\n" << "GRed rank: " << rank << "\n";; // DEBUG

	// on reassocie la matrice G et GRed
	for (int k = 0; k < nb_NonPivots; k++) {
	    for (int l = 0; l < GColumns[i]; l++) {
		Row_NoPivot = GRedRowsNoPivot[k];
		G[Row_NoPivot][l] = GRed[k][l];
	    }
	}

	// Tri

	// On associe la matrice G a Gtemp
	// GRowFinal;
	// GColumnsFinal;
	G.SetDims(GRows[i], GColumns[i]); // detruit G car G_{i}Rows > G_{i-1}Rows
	// PROBLEME ICI
	// UNION(mons(G), mons(Gtemp)) = ?
	for (int j = 0; j < TFinal[i].length(); j++) {
	    indice0 = TFinal[i][j][0][0];
	    indice1 = TFinal[i][j][0][1];
	    indice2 = TFinal[i][j][1][0];
	    indice3 = TFinal[i][j][1][1];
	    G[indice2][indice3] = G[indice0][indice1];
	}

	// Tri
	
    }

    return 0;
}

/* Strategy: line per line from down to top, browse the coefs of the
   line until we find the first coefs, check if it's 1, if it is
   reduce the superior coeffs to 0, if not go to next line */
int PivotsReduction(Mat<ZZ_p>& M, int MRows, int MColumns) {
    int j;
    int nbpivots = 0;
    cout << M << "\n\n";
    for (int i = MRows - 1; i >= 0; i--) {
	j = 0;
	// cout << i << " "; // DEBUG


	while ((M[i][j] == 0) && (j < MColumns)) {
	    j++;
	}
	// cout << j << " \n"; // DEBUG

	if (M[i][j] == 1) {
	    nbpivots++;
	    for (int k = 0; k < i; k++) {
		for (int l = j + 1; l < MColumns; l++) {
		    M[k][l] = M[k][l] - M[i][l]*M[k][j]; // 5 = k,j
		    // cout << M[i][l]*M[k][j] << " I J \n";
		}
		M[k][j] = 0;
	    }
	} // else do nothing

    }
    return nbpivots;
}
