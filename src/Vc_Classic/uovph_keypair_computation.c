/// @file rainbow_keypair_computation.c
/// @brief Implementations for functions in rainbow_keypair_computation.h
///

#include "rainbow_keypair.h"
#include "rainbow_keypair_computation.h"
#include "uovph_keypair.h"
#include "uovph_keypair_computation.h"

#include "blas_comm.h"
#include "blas.h"
#include "rainbow_blas.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utils_malloc.h"

////////////////////////////////////////////////////////////////



// stockage: elnt1 des Q1, elnt1 des Q2, elnt2 des Q1, elnt1 des Q2 etc.
// i, j sont des indexes globaux de toute la matrice pk (pas juste des blocs)
void uovph_extcpk_to_pk( uovph_pk_t * pk , const uovph_ext_cpk_t * cpk )
{
    const unsigned char * idx_l1 = cpk->l1_Q1;
    const unsigned char * idx_l2 = cpk->l2_Q1;
    for(unsigned i=0;i<_V;i++) {
	for(unsigned j=i;j<_V;j++) {
	    unsigned pub_idx = idx_of_trimat(i,j,_PUB_N);
	    memcpy( & pk->pk[ _PUB_M_BYTE*pub_idx ]            , idx_l1 , _O_BYTE );
	    memcpy( (&pk->pk[ _PUB_M_BYTE*pub_idx ]) + _O_BYTE , idx_l2 , _O_BYTE );
	    idx_l1 += _O_BYTE;
	    idx_l2 += _O_BYTE;
	}
    }
    idx_l1 = cpk->l1_Q2;
    idx_l2 = cpk->l2_Q2;
    for(unsigned i=0;i<_V;i++) {
	for(unsigned j=_V;j<_PUB_N;j++) {
	    unsigned pub_idx = idx_of_trimat(i,j,_PUB_N);
	    memcpy( & pk->pk[ _PUB_M_BYTE*pub_idx ]            , idx_l1 , _O_BYTE );
	    memcpy( (&pk->pk[ _PUB_M_BYTE*pub_idx ]) + _O_BYTE , idx_l2 , _O_BYTE );
	    idx_l1 += _O_BYTE;
	    idx_l2 += _O_BYTE;
	}
    }
    idx_l1 = cpk->l1_Q4;
    idx_l2 = cpk->l2_Q4;
    for(unsigned i=_V;i<_PUB_N;i++) {
        for(unsigned j=i;j<_PUB_N;j++) {
            unsigned pub_idx = idx_of_trimat(i,j,_PUB_N);
            memcpy( & pk->pk[ _PUB_M_BYTE*pub_idx ]            , idx_l1 , _O_BYTE );
            memcpy( (&pk->pk[ _PUB_M_BYTE*pub_idx ]) + _O_BYTE , idx_l2 , _O_BYTE );
            idx_l1 += _O_BYTE;
            idx_l2 += _O_BYTE;
	}
    }
}



////////////////////////////////////////////////////////////////////////////



// Choosing implementations depends on the macros: _BLAS_SSE_ and _BLAS_AVX2_
#if defined(_BLAS_SSE_) || defined(_BLAS_AVX2_)
#include "rainbow_keypair_computation_simd.h"
#define uovph_calculate_Q_from_F_impl        uovph_calculate_Q_from_F_simd
#define uovph_calculate_F_from_Q_impl        uovph_calculate_F_from_Q_simd
/* #define uovph_calculate_Q_from_F_cyclic_impl uovph_calculate_Q_from_F_cyclic_simd */

#else

#define uovph_calculate_Q_from_F_ref        uovph_calculate_Q_from_F_impl
#define uovph_calculate_F_from_Q_ref        uovph_calculate_F_from_Q_impl
/* #define uovph_calculate_Q_from_F_cyclic_ref uovph_calculate_Q_from_F_cyclic_impl */



/////////////////////////////////////////////////////////

// Fs et Ts prennent le meme sk en input
static
void uovph_calculate_Q_from_F_ref( uovph_ext_cpk_t * Qs, const uovph_sk_t * Fs , const uovph_sk_t * Ts )
{
/*
    Layer 1
    Computing :
    Q1 = F1
    Q2 = (F1* T1 + F2) + F1 * T1tr
    Q4 = UT( T1tr* (F1 * T1 + F2) )
*/
    // const unsigned char * t2 = Ts->t4;

    memcpy( Qs->l1_Q1 , Fs->l1_F1 , _O_BYTE * N_TRIANGLE_TERMS(_V) );

    memcpy( Qs->l1_Q2 , Fs->l1_F2 , _O_BYTE * _V * _O );
    batch_trimat_madd( Qs->l1_Q2 , Fs->l1_F1 , Ts->t1 , _V, _V_BYTE , _O, _O_BYTE );    // F1*T1 + F2

    memset( Qs->l1_Q4 , 0 , _O_BYTE * N_TRIANGLE_TERMS(_O) );

    unsigned char _ALIGN_(32) tempQ[_O_BYTE * _O * _O];

    memset( tempQ , 0 , _O_BYTE * _O * _O );   // l1_Q4
    batch_matTr_madd( tempQ , Ts->t1 , _V, _V_BYTE, _O, Qs->l1_Q2, _O, _O_BYTE );  // t1_tr*(F1*T1 + F2)
    UpperTrianglize( Qs->l1_Q4 , tempQ , _O, _O_BYTE );    // UT( ... )   // Q4

    batch_trimatTr_madd( Qs->l1_Q2 , Fs->l1_F1 , Ts->t1 , _V, _V_BYTE , _O, _O_BYTE );    // Q2

/*
    layer 2
    Computing:
    Q1 = F1
    Q2 = F1_F1T*T1 + F2
    Q4 = UT( T1tr( F1*T1 + F2 ) + F4 )
*/
    memcpy( Qs->l2_Q1 , Fs->l2_F1 , _O_BYTE * N_TRIANGLE_TERMS(_V) );

    memcpy( Qs->l2_Q2 , Fs->l2_F2 , _O_BYTE * _V * _O );
    batch_trimat_madd( Qs->l2_Q2 , Fs->l2_F1 , Ts->t1 , _V, _V_BYTE , _O, _O_BYTE );      // F1*T1 + F2

    memcpy( Qs->l2_Q4 , Fs->l2_F4 , _O_BYTE * N_TRIANGLE_TERMS(_O) );
    memset( tempQ , 0 , _O_BYTE * _O * _O );                                               // l2_Q5
    batch_matTr_madd( tempQ , Ts->t1 , _V, _V_BYTE, _O, Qs->l2_Q2, _O, _O_BYTE );        // t1_tr*(F1*T1 + F2)
    UpperTrianglize( Qs->l2_Q4 , tempQ , _O, _O_BYTE );                                     // UT( ... )   // Q5

    batch_trimatTr_madd( Qs->l2_Q2 , Fs->l2_F1 , Ts->t1 , _V, _V_BYTE , _O, _O_BYTE );    // Q2

    memset( tempQ , 0 , _O_BYTE * _O * _O);
}





/////////////////////////////////////////////////////

/* static */
/* void uovph_calculate_F_from_Q_ref( uovph_sk_t * Fs , const uovph_sk_t * Qs , const uovph_sk_t * Ts ) */
/* { */
/*     // Layer 1 */
/*     // F_sk.l1_F1s[i] = Q_pk.l1_F1s[i] */
/*     memcpy( Fs->l1_F1 , Qs->l1_F1 , _O1_BYTE * N_TRIANGLE_TERMS(_V1) ); */

/*     // F_sk.l1_F2s[i] = ( Q_pk.l1_F1s[i] + Q_pk.l1_F1s[i].transpose() ) * T_sk.t1 + Q_pk.l1_F2s[i] */
/*     memcpy( Fs->l1_F2 , Qs->l1_F2 , _O1_BYTE * _V1*_O1 ); */
/*     batch_2trimat_madd( Fs->l1_F2 , Qs->l1_F1 , Ts->t1 , _V1, _V1_BYTE , _O1, _O1_BYTE ); */

/* /\* */
/*     Layer 2 */
/*     computations: */

/*     F_sk.l2_F1s[i] = Q_pk.l2_F1s[i] */

/*     Q1_T1 = Q_pk.l2_F1s[i]*T_sk.t1 */
/*     F_sk.l2_F2s[i] =              Q1_T1 + Q_pk.l2_F2s[i]     + Q_pk.l2_F1s[i].transpose() * T_sk.t1 */
/*     F_sk.l2_F5s[i] = UT( t1_tr* ( Q1_T1 + Q_pk.l2_F2s[i] ) ) + Q_pk.l2_F5s[i] */

/*     Q1_Q1T_T4 =  (Q_pk.l2_F1s[i] + Q_pk.l2_F1s[i].transpose()) * t4 */
/*     #Q1_Q1T_T4 =  Q1_Q1T * t4 */
/*     Q2_T3 = Q_pk.l2_F2s[i]*T_sk.t3 */
/*     F_sk.l2_F3s[i] =           Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] */
/*     F_sk.l2_F6s[i] = t1_tr * ( Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] ) */
/* 		    +  Q_pk.l2_F2s[i].transpose() * t4 */
/* 		    + (Q_pk.l2_F5s[i] + Q_pk.l2_F5s[i].transpose())*T_sk.t3   + Q_pk.l2_F6s[i] */

/* *\/ */
/*     memcpy( Fs->l2_F1 , Qs->l2_F1 , _O2_BYTE * N_TRIANGLE_TERMS(_V1) );        // F_sk.l2_F1s[i] = Q_pk.l2_F1s[i] */


/*     // F_sk.l2_F2s[i] =              Q1_T1 + Q_pk.l2_F2s[i]     + Q_pk.l2_F1s[i].transpose() * T_sk.t1 */
/*     // F_sk.l2_F5s[i] = UT( t1_tr* ( Q1_T1 + Q_pk.l2_F2s[i] ) ) + Q_pk.l2_F5s[i] */
/*     memcpy( Fs->l2_F2 , Qs->l2_F2 , _O2_BYTE * _V1*_O1 ); */
/*     batch_trimat_madd( Fs->l2_F2 , Qs->l2_F1 , Ts->t1 , _V1, _V1_BYTE , _O1, _O2_BYTE );    // Q1_T1+ Q2 */

/*     unsigned char _ALIGN_(32) tempQ[_O1*_O1*_O2_BYTE] = {0}; */
/*     batch_matTr_madd( tempQ , Ts->t1 , _V1, _V1_BYTE, _O1, Fs->l2_F2, _O1, _O2_BYTE );     // t1_tr*(Q1_T1+Q2) */
/*     memcpy( Fs->l2_F5, Qs->l2_F5, _O2_BYTE * N_TRIANGLE_TERMS(_O1) );                      // F5 */
/*     UpperTrianglize( Fs->l2_F5 , tempQ , _O1, _O2_BYTE );                                  // UT( ... ) */
/*     memset( tempQ , 0 , _O1*_O1*_O2_BYTE ); */

/*     batch_trimatTr_madd( Fs->l2_F2 , Qs->l2_F1 , Ts->t1 , _V1, _V1_BYTE , _O1, _O2_BYTE );  // F2 = Q1_T1 + Q2 + Q1^tr*t1 */

/*     // Q1_Q1T_T4 =  (Q_pk.l2_F1s[i] + Q_pk.l2_F1s[i].transpose()) * t4 */
/*     // Q2_T3 = Q_pk.l2_F2s[i]*T_sk.t3 */
/*     // F_sk.l2_F3s[i] =           Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] */
/*     memcpy( Fs->l2_F3 , Qs->l2_F3 , _V1*_O2*_O2_BYTE ); */
/*     batch_2trimat_madd( Fs->l2_F3 , Qs->l2_F1 , Ts->t4 , _V1, _V1_BYTE , _O2, _O2_BYTE );   // Q1_Q1T_T4 */
/*     batch_mat_madd( Fs->l2_F3 , Qs->l2_F2 , _V1, Ts->t3 , _O1, _O1_BYTE , _O2, _O2_BYTE );  // Q2_T3 */

/*     // F_sk.l2_F6s[i] = t1_tr * ( Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] ) */
/*     //                +  Q_pk.l2_F2s[i].transpose() * t4 */
/*     //                + (Q_pk.l2_F5s[i] + Q_pk.l2_F5s[i].transpose())*T_sk.t3   + Q_pk.l2_F6s[i] */
/*     memcpy( Fs->l2_F6 , Qs->l2_F6 , _O1*_O2*_O2_BYTE ); */
/*     batch_matTr_madd( Fs->l2_F6 , Ts->t1 , _V1, _V1_BYTE, _O1, Fs->l2_F3, _O2, _O2_BYTE );  // t1_tr * ( Q1_Q1T_T4 + Q2_T3 + Q_pk.l2_F3s[i] ) */
/*     batch_2trimat_madd( Fs->l2_F6 , Qs->l2_F5 , Ts->t3, _O1, _O1_BYTE, _O2, _O2_BYTE );     // (Q_pk.l2_F5s[i] + Q_pk.l2_F5s[i].transpose())*T_sk.t3 */
/*     batch_bmatTr_madd( Fs->l2_F6 , Qs->l2_F2, _O1, Ts->t4, _V1, _V1_BYTE, _O2, _O2_BYTE ); */

/* } */


//////////////////////////////////////////////////////////////////////////////////////////////////

/* static */
/* void calculate_Q_from_F_cyclic_ref( cpk_t * Qs, const sk_t * Fs , const sk_t * Ts ) */
/* { */
/* // Layer 1: Computing Q5, Q3, Q6, Q9 */

/* //  Q_pk.l1_F5s[i] = UT( T1tr* (F1 * T1 + F2) ) */
/*     const unsigned char * t2 = Ts->t4; */

/* // assuming _O2 >= _O1 */
/* #define _SIZE_BUFFER_F2 (_O2_BYTE * _V1 * _O2) */
/*     unsigned char _ALIGN_(32) buffer_F2[_SIZE_BUFFER_F2]; */
/*     memcpy( buffer_F2 , Fs->l1_F2 , _O1_BYTE * _V1 * _O1 ); */
/*     batch_trimat_madd( buffer_F2 , Fs->l1_F1 , Ts->t1 , _V1, _V1_BYTE , _O1, _O1_BYTE );      // F1*T1 + F2 */

/* // assuming _O2 >= _O1 */
/* #define _SIZE_BUFFER_F3 (_O2_BYTE * _V1 * _O2) */
/*     unsigned char _ALIGN_(32) buffer_F3[_SIZE_BUFFER_F3]; */
/*     memset( buffer_F3 , 0 , _O1_BYTE * _V1 * _O2 ); */
/*     batch_matTr_madd( buffer_F3 , Ts->t1 , _V1, _V1_BYTE, _O1, buffer_F2, _O1, _O1_BYTE );  // T1tr*(F1*T1 + F2) , release buffer_F2 */
/*     memset( Qs->l1_Q5 , 0 , _O1_BYTE * N_TRIANGLE_TERMS(_O1) ); */
/*     UpperTrianglize( Qs->l1_Q5 , buffer_F3 , _O1, _O1_BYTE );                        // UT( ... )   // Q5 , release buffer_F3 */

/* /\* */
/*     F1_T2     = F1 * t2 */
/*     F2_T3     = F2 * t3 */
/*     F1_F1T_T2 + F2_T3 = F1_T2 + F2_T3 + F1tr * t2 */
/*     Q_pk.l1_F3s[i] =         F1_F1T_T2 + F2_T3 */
/*     Q_pk.l1_F6s[i] = T1tr* ( F1_F1T_T2 + F2_T3 ) + F2tr * t2 */
/*     Q_pk.l1_F9s[i] = UT( T2tr* ( F1_T2 + F2_T3 ) ) */
/* *\/ */
/*     memset( Qs->l1_Q3 , 0 , _O1_BYTE * _V1 * _O2 ); */
/*     memset( Qs->l1_Q6 , 0 , _O1_BYTE * _O1 * _O2 ); */
/*     memset( Qs->l1_Q9 , 0 , _O1_BYTE * N_TRIANGLE_TERMS(_O2) ); */

/*     batch_trimat_madd( Qs->l1_Q3 , Fs->l1_F1 , t2 , _V1, _V1_BYTE , _O2, _O1_BYTE );        // F1*T2 */
/*     batch_mat_madd( Qs->l1_Q3 , Fs->l1_F2 , _V1, Ts->t3 , _O1, _O1_BYTE , _O2, _O1_BYTE );  // F1_T2 + F2_T3 */

/*     memset( buffer_F3 , 0 , _O1_BYTE * _V1 * _O2 ); */
/*     batch_matTr_madd( buffer_F3 , t2 , _V1, _V1_BYTE, _O2, Qs->l1_Q3, _O2, _O1_BYTE );    // T2tr *  ( F1_T2 + F2_T3 ) */
/*     UpperTrianglize( Qs->l1_Q9 , buffer_F3 , _O2 , _O1_BYTE );                            // Q9 , release buffer_F3 */

/*     batch_trimatTr_madd( Qs->l1_Q3 , Fs->l1_F1 , t2 , _V1, _V1_BYTE, _O2, _O1_BYTE );       // F1_F1T_T2 + F2_T3  // Q3 */

/*     batch_bmatTr_madd( Qs->l1_Q6 , Fs->l1_F2, _O1, t2, _V1, _V1_BYTE, _O2, _O1_BYTE );      // F2tr*T2 */
/*     batch_matTr_madd( Qs->l1_Q6 , Ts->t1, _V1, _V1_BYTE, _O1, Qs->l1_Q3, _O2, _O1_BYTE );   // Q6 */
/* /\* */
/*     Layer 2 */
/*     Computing Q9: */

/*     F1_T2     = F1 * t2 */
/*     F2_T3     = F2 * t3 */
/*     Q9 = UT( T2tr*( F1*T2 + F2*T3 + F3 )  +  T3tr*( F5*T3 + F6 ) ) */
/* *\/ */
/* #if _SIZE_BUFFER_F3 < _O2_BYTE * _V1 * _O2 */
/* error: incorrect buffer size. */
/* #endif */
/*     memcpy( buffer_F3 , Fs->l2_F3 , _O2_BYTE * _V1 * _O2 ); */
/*     batch_trimat_madd( buffer_F3 , Fs->l2_F1 , t2 , _V1, _V1_BYTE , _O2, _O2_BYTE );       // F1*T2 + F3 */
/*     batch_mat_madd( buffer_F3 , Fs->l2_F2 , _V1, Ts->t3 , _O1, _O1_BYTE , _O2, _O2_BYTE ); // F1_T2 + F2_T3 + F3 */

/* #if _SIZE_BUFFER_F2 < _O2_BYTE * _V1 * _O2 */
/* error: incorrect buffer size. */
/* #endif */
/*     memset( buffer_F2 , 0 , _O2_BYTE * _V1 * _O2 ); */
/*     batch_matTr_madd( buffer_F2 , t2 , _V1, _V1_BYTE, _O2, buffer_F3, _O2, _O2_BYTE );   // T2tr * ( ..... )  , release buffer_F3 */

/* #if _SIZE_BUFFER_F3 < _O2_BYTE*_O1*_O2 */
/* error: incorrect buffer size. */
/* #endif */
/*     memcpy( buffer_F3 , Fs->l2_F6 , _O2_BYTE * _O1 *_O2 ); */
/*     batch_trimat_madd( buffer_F3 , Fs->l2_F5 , Ts->t3 , _O1, _O1_BYTE, _O2, _O2_BYTE );     // F5*T3 + F6 */

/*     batch_matTr_madd( buffer_F2 , Ts->t3 , _O1, _O1_BYTE, _O2, buffer_F3, _O2, _O2_BYTE ); // T2tr*( ..... ) + T3tr*( ..... ) */
/*     memset( Qs->l2_Q9 , 0 , _O2_BYTE * N_TRIANGLE_TERMS(_O2) ); */
/*     UpperTrianglize( Qs->l2_Q9 , buffer_F2 , _O2 , _O2_BYTE );                              // Q9 */

/*     memset( buffer_F2 , 0 , _SIZE_BUFFER_F2 ); */
/*     memset( buffer_F3 , 0 , _SIZE_BUFFER_F2 ); */
/* } */



#endif   //  #if defined(_BLAS_SSE_) || defined(_BLAS_AVX2_)


///////////////////////////////////////////////////////////////////////





void uovph_calculate_Q_from_F( uovph_ext_cpk_t * Qs, const uovph_sk_t * Fs , const uovph_sk_t * Ts )
{
    uovph_calculate_Q_from_F_impl( Qs , Fs , Ts );
}


/* #if defined(_SUPERCOP_) */


/* static inline */
/* void _uovph_calculate_F_from_Q( uovph_sk_t * Fs , const uovph_sk_t * Qs , const uovph_sk_t * Ts ) */
/* { */
/*     sk_t _Qs; */
/*     memcpy( &_Qs , Qs , sizeof(sk_t) ); */
/*     if( Fs != Ts ) memcpy( Fs , Ts , sizeof(Ts->sk_seed)+sizeof(Ts->s1)+sizeof(Ts->t1)+sizeof(Ts->t4)+sizeof(Ts->t3)); */
/*     uovph_calculate_F_from_Q_impl( Fs , &_Qs , Ts ); */
/*     memset( &_Qs , 0 , sizeof(sk_t) ); */
/* } */

/* //IF_CRYPTO_CORE:#include "crypto_core.h" */

/* // exported calculate_F_from_Q_impl() */
/* int uovph_crypto_core(unsigned char *outbytes,const unsigned char *inbytes,const unsigned char *kbytes,const unsigned char *cbytes) */
/* { */
/*     (void) cbytes; */
/*     _uovph_calculate_F_from_Q( (sk_t*)outbytes , (const sk_t*)inbytes , (const sk_t*) kbytes ); */
/*     return 0; */
/* } */

/* #else */

/* void uovph_calculate_F_from_Q( uovph_sk_t * Fs , const uovph_sk_t * Qs , const uovph_sk_t * Ts ) */
/* { */
/*     uovph_calculate_F_from_Q_impl( Fs , Qs , Ts ); */
/* } */

/* #endif */


/* void uovph_calculate_Q_from_F_cyclic( cpk_t * Qs, const sk_t * Fs , const sk_t * Ts ) */
/* { */
/*     uovph_calculate_Q_from_F_cyclic_impl( Qs , Fs , Ts ); */
/* } */
