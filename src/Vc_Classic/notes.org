#+TITLE: Note on implementation


api_config.h
rainbow_config.h


*BRANCHES*
(/rainbow-*.c -> api.h -> sign.c)
crypto_sign_keypair(unsigned char *pk, unsigned char *sk) //  - generate_keypair in rainbow_keypair.c
crypto_sign(unsigned char *sm, unsigned long long *smlen, const unsigned char *m, unsigned long long mlen, const unsigned char *sk) - rainbow_sign in rainbow.c
crypto_sign_open(unsigned char *m, unsigned long long *mlen,const unsigned char *sm, unsigned long long smlen,const unsigned char *pk) - rainbow_verify in rainbow.c

uovph_keypair.c
uovph_sign.c
uovph_verify.c

_gf256v_madd_u32 (blas_u32)


*GENKEY*
#+begin_src C
uint8_t *_sk = (uint8_t*)malloc( CRYPTO_SECRETKEYBYTES );
uint8_t *_pk = (uint8_t*)malloc( CRYPTO_PUBLICKEYBYTES );
crypto_sign_keypair( _pk, _sk );
#define CRYPTO_SECRETKEYBYTES sizeof(uovph_sk_t)
#define CRYPTO_PUBLICKEYBYTES sizeof(uovph_pk_t)
uovph_generate_keypair( (uovph_pk_t*) pk , (uovph_sk_t*) sk , sk_seed );
#+end_src


uovph_calculate_Q_from_F( pk, sk , sk );   // compute the public key in uovph_ext_cpk_t format. // -> calculate_Q_from_F_ref
void uovph_calculate_Q_from_F( uovph_ext_cpk_t * Qs, const uovph_sk_t * Fs , const uovph_sk_t * Ts );
L1_F1s/2/5
F1tr - transpose de F1
UT = upper triangulize

Layer 1
Computing :
Q1 = F1
Q2 = (F1* T1 + F2) + F1tr * T1
Q5 = UT( T1tr * (F1 * T1 + F2) )
F1_T2     = F1 * T2
F2_T3     = F2 * T3
F1_F1T_T2 + F2_T3 = F1_T2 + F2_T3 + F1tr * T2
Q3 =         F1_F1T_T2 + F2_T3
Q6 = T1tr* ( F1_F1T_T2 + F2_T3 ) + F2tr * T2
Q9 = UT( T2tr* ( F1_T2 + F2_T3 ) )


layer 2
Computing:
Q1 = F1
Q2 = F1_F1T*T1 + F2
Q5 = UT( T1tr( F1*T1 + F2 )  + F5 )
F1_T2     = F1 * t2
F2_T3     = F2 * t3
F1_F1T_T2 + F2_T3 = F1_T2 + F2_T3 + F1tr * t2
Q3 =        F1_F1T*T2 + F2*T3 + F3
Q9 = UT( T2tr*( F1*T2 + F2*T3 + F3 )  +      T3tr*( F5*T3 + F6 ) )
Q6 = T1tr*( F1_F1T*T2 + F2*T3 + F3 )  + F2Tr*T2 + F5_F5T*T3 + F6


*SIGN*
#+begin_src C
#define CRYPTO_BYTES _SIGNATURE_BYTE // api.h
#define _SIGNATURE_BYTE (_PUB_N_BYTE + _SALT_BYTE ) // lengh signature
#+end_src
