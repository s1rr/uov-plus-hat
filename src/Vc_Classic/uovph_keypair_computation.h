/// @file rainbow_keypair_computation.h
/// @brief Functions for calculating pk/sk while generating keys.
///
/// Defining an internal structure of public key.
/// Functions for calculating pk/sk for key generation.
///

#ifndef _UOVPH_KEYPAIR_COMP_H_
#define _UOVPH_KEYPAIR_COMP_H_

#include "rainbow_keypair.h"
#include "uovph_keypair.h"

//IF_CRYPTO_CORE:define CRYPTO_NAMESPACE

#ifdef  __cplusplus
extern  "C" {
#endif



/// @brief The (internal use) public key for rainbow
///
/// The (internal use) public key for rainbow. The public
/// polynomials are divided into l1_Q1, l1_Q2, ... l1_Q9,
/// l2_Q1, .... , l2_Q9.
///
typedef
struct uovph_extend_publickey {
    unsigned char l1_Q1[_PUB_M_BYTE * N_TRIANGLE_TERMS(_V)]; // calcul par bloc
    unsigned char l1_Q2[_PUB_M_BYTE * _V*_O];
    unsigned char l1_Q4[_PUB_M_BYTE * N_TRIANGLE_TERMS(_O)];

    unsigned char l2_Q1[_PUB_M_BYTE * N_TRIANGLE_TERMS(_V)];
    unsigned char l2_Q2[_PUB_M_BYTE * _V*_O];
    unsigned char l2_Q4[_PUB_M_BYTE * N_TRIANGLE_TERMS(_O)];
} uovph_ext_cpk_t;



///
/// @brief converting formats of public keys : from uovph_ext_cpk_t version to uovph_pk_t
///
/// @param[out] pk       - the classic public key.
/// @param[in]  cpk      - the internel public key.
///
void uovph_extcpk_to_pk( uovph_pk_t * pk , const uovph_ext_cpk_t * cpk );


/////////////////////////////////////////////////

///
/// @brief Computing public key from secret key
///
/// @param[out] Qs       - the public key
/// @param[in]  Fs       - parts of the secret key: l1_F1, l1_F2, l2_F1, l2_F2, l2_F3, l2_F5, l2_F6
/// @param[in]  Ts       - parts of the secret key: T1, T4, T3
///
void uovph_calculate_Q_from_F( uovph_ext_cpk_t * Qs, const uovph_sk_t * Fs , const uovph_sk_t * Ts );

#include <rng.h> // the macro _SUPERCOP_ might be defined in rng.h

#if !defined(_SUPERCOP_)
///
/// @brief Computing parts of the sk from parts of pk and sk
///
/// @param[out] Fs       - parts of the sk: l1_F1, l1_F2, l2_F1, l2_F2, l2_F3, l2_F5, l2_F6
/// @param[in]  Qs       - parts of the pk: l1_Q1, l1_Q2, l2_Q1, l2_Q2, l2_Q3, l2_Q5, l2_Q6
/// @param[in]  Ts       - parts of the sk: T1, T4, T3
///
void uovph_calculate_F_from_Q( uovph_sk_t * Fs , const uovph_sk_t * Qs , const uovph_sk_t * Ts );
#endif

///
/// @brief Computing parts of the pk from the secret key
///
/// @param[out] Qs       - parts of the pk: l1_Q3, l1_Q5, l2_Q6, l1_Q9, l2_Q9
/// @param[in]  Fs       - parts of the sk: l1_F1, l1_F2, l2_F1, l2_F2, l2_F3, l2_F5, l2_F6
/// @param[in]  Ts       - parts of the sk: T1, T4, T3
///
/* void calculate_Q_from_F_cyclic( cpk_t * Qs, const uovph_sk_t * Fs , const uovph_sk_t * Ts ); */






#ifdef  __cplusplus
}
#endif

#endif  // _RAINBOW_KEYPAIR_COMP_H_

