# UOV plus hat implementation and related code

`src/`: Implementation of UOV variant UOV plus hat in C based on the Rainbow submission code https://github.com/fast-crypto-lab/rainbow-submission-round2. I used the first set of parameters in https://eprint.iacr.org/2022/203.pdf. What is left to implement is the resolution of the small quadratic system with grobner based solving algorithm using an external library.

`uov-plus-hat.magma` and `f4-precalcul.magma`: You'll find an uov plus hat implementation in magma.

`precomputation/`: you'll find the result of our attempt to accelerate the scheme, in magma and in c++.

`Stage_orange___report.pdf`: Report on my work done during the internship

`Stage_orange___presentation.pdf`: Presentation given at the end of my internship

## License
GPLv3
